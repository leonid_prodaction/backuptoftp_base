﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupToFTP
{
    public partial class Form1 : Form
    {
        private Setting setting;

        public Form1()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SettingManager.Save(setting);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            setting = SettingManager.Load();
            dataSetting.Add(setting);

            var bindingSource = new BindingSource();
            bindingSource.DataSource = setting.SettingLineBackup;
            dgSetting.AutoGenerateColumns = true;
            dgSetting.DataSource = bindingSource;

        }

        private void dgSetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataSetting_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataSetting_BindingComplete(object sender, BindingCompleteEventArgs e)
        {

        }

        private void dataSettingLinesBackup_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dgSetting_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
        //    foreach (DataGridViewRow row in dgSetting.Rows)
        //    {
        //        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(row.Cells["typeLog"]);
        //        cell.DataSource = new string[] { "10", "30" };
        //    }
        }
    }
}
