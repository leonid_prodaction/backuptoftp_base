﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace BackupToFTP
{
    class MailLogger : IMailLogger
    {
        private List<KeyValuePair<string, bool>> logString;
        private bool ReadyToSend;

        public MailLogger()
        {
            logString = new List<KeyValuePair<string, bool>>();
            ReadyToSend = false;
        }


        private void AddLog(string str, bool isError = false)
        {
            logString.Add(new KeyValuePair<string, bool>(str, isError));
        }

        public void AddTitle(string str)
        {
            AddLog("<b>" + str + "</b>");
        }

        public void AddInfo(string str)
        {
            AddLog("  " + str);
            ReadyToSend = true;
        }

        public void AddError(string str)
        {
            AddLog("  !" + str, true);
            ReadyToSend = true;
        }

        private bool isError()
        {
            return logString.Any(x => x.Value == true);
        }

        public void SendLog(Setting Setting, string Name = "")
        {
            if (!ReadyToSend) return;

            MailMessage m = new MailMessage();
            m.From = new MailAddress(Setting.MailFrom);
            Setting.MailAdressTo.Split(new char[] { ';', ',', ' ' }).ToList().ForEach(x => m.To.Add(new MailAddress(x)));
            // тема письма
            m.Subject = (isError() ? "FTP ERROR " : "FTP SUCCESSFUL ") + Name + " from " + Setting.ServerName;
            // текст письма
            m.Body = GetHTML();
            // письмо представляет код html
            m.IsBodyHtml = true;
            // адрес smtp-сервера и порт, с которого будем отправлять письмо
            SmtpClient smtp = new SmtpClient(Setting.SMTPserver, Setting.MailPort);
            // логин и пароль
            smtp.Credentials = new NetworkCredential(Setting.MailFrom, Setting.MailPassword);
            smtp.EnableSsl = true;
            smtp.Send(m);
        }

        private string GetHTML()
        {
            string str = "";
            logString.ForEach(x => str += (x.Value ? "<p style=\"color: red;\">" + x.Key + "</p>" : "<p style=\"color: green;\">" + x.Key + "</p>"));
            return str;
        }

        public void Clear()
        {
            logString.Clear();
        }
    }
}
