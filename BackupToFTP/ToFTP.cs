﻿using FluentFTP;
using FluentFTP.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Linq;
using Microsoft.Extensions.Logging;
using DebounceThrottle;

namespace BackupToFTP
{
    class ToFTP
    {
        private Setting Setting;
        private IMailLogger mailLoger;
        private ILogger logger;

        public void Run(Func<IMailLogger> factoryMailLogger, Func<ILogger> factoryLogger)
        {
            logger = factoryLogger.Invoke();
            mailLoger = factoryMailLogger.Invoke();

            Setting = SettingManager.Load();
            Setting.SettingLineBackup.ForEach(executeSetting);
           
        }

        private void executeSetting(SettingLineBackup s)
        {
            mailLoger.Clear();
            mailLoger.AddTitle("Running exp: ." + s.EXP);
            logger.LogInformation($"Running exp: . {s.EXP}");

            //очистим фтп от старых бэкапов
            mailLoger.AddTitle("-> clear remote storage");
            logger.LogInformation($"-> clear remote storage");
            clearRemoteStorage(s);
            //очистим локально от старых архивов
            mailLoger.AddTitle("-> clear local storage");
            logger.LogInformation($"-> clear local storage");
            clearLocalStorage(s);


            if (Directory.Exists(s.PathBackup))
            {
                mailLoger.AddTitle("-> Archiving and sending");
                logger.LogInformation($"-> Archiving and sending");
                List<FileInfo> listFindFiles = new List<FileInfo>();
                findFiles(s.PathBackup,s, listFindFiles);
                if (listFindFiles.Count != 0)
                {
                    listFindFiles.ForEach(f =>
                    {
                        if (s.UseZip)
                        {
                            var file2Ftp = toZip(f, s);
                            if (file2Ftp != null)
                            {
                                deleteFile(f);
                            }
                            fileToFTP(file2Ftp, s);
                        }
                        else
                        {
                            var file2Ftp = renameToTemp(f);
                            fileToFTP(file2Ftp, s);
                        }
                        
                    });
                }

                //если что то не отправили 
                mailLoger.AddTitle("-> Check not sent");
                logger.LogInformation($"-> Check not sent");
                List<FileInfo> listFindFilesTmp = new List<FileInfo>();
                findFiles(s.PathBackup, tmpExt(), listFindFilesTmp);
                listFindFilesTmp.ForEach(f => fileToFTP(f, s));
            }
            else
            {
                mailLoger.AddInfo("Directory " + s.PathBackup + " not existing");
                logger.LogInformation($"Directory {s.PathBackup} not existing");
            }
            if(s.UseMail)
                mailLoger.SendLog(Setting,s.EXP);
        }

        private string getCompleteFileName(string tmpFileName,bool local)
        {
            var str = tmpFileName.Replace("." + tmpExt(), "");
            if (!str.Contains("rar") && local)
            {
                str = str + ".complete";
            }
            return str;
        }

        private string getTmpFileName(string fileName)
        {
            return fileName + "." + tmpExt();
        }

        private FileInfo renameToComplete(FileInfo fileInfo)
        {
            logger.LogInformation($"renameToComplete {fileInfo.FullName}");
            if (File.Exists(getCompleteFileName(fileInfo.FullName,true)))
            {
                File.Delete(getCompleteFileName(fileInfo.FullName, true));
            }
            var newFile = new FileInfo(getCompleteFileName(fileInfo.FullName, true));
            File.Move(fileInfo.FullName, newFile.FullName);
            return newFile;
        }

        private FileInfo renameToTemp(FileInfo fileInfo)
        {
            logger.LogInformation($"renameToTemp {fileInfo.FullName}");
            if (File.Exists(getTmpFileName(fileInfo.FullName)))
            {
                File.Delete(getTmpFileName(fileInfo.FullName));
            }
            var newFile = new FileInfo(getTmpFileName(fileInfo.FullName));
            File.Move(fileInfo.FullName, newFile.FullName);
            return newFile;
        }

        private void deleteFile(FileInfo f)
        {
            logger.LogInformation($"deleteFile {f.FullName}");
            try
            {
                File.Delete(f.FullName);
            }
            catch (Exception e)
            {
                mailLoger.AddError("ERROR " + e.Message + " </br> " + e.StackTrace);
                logger.LogError($"{e.Message}  { e.StackTrace}");
            }

        }

        private void findFiles(string pathFolder, SettingLineBackup s, List<FileInfo> lFi)
        {
            findFiles(pathFolder, s.EXP, lFi);
        }

        private void findFiles(string pathFolder, string EXP, List<FileInfo> result)
        {
            Directory.GetDirectories(pathFolder).ToList().ForEach(f => findFiles(f, EXP, result));
            Directory.GetFiles(pathFolder).ToList().ForEach(f =>
            {
                var fi = new FileInfo(f);
                if (fi.Extension == EXP || fi.Extension == "." + EXP || EXP == "*")
                {
                    try
                    {
                        var fs = fi.OpenWrite();
                        fs.Close();
                        result.Add(fi);
                    }
                    catch //файл занят
                    {

                    }
                }
            });
        }

        private FileInfo toZip(FileInfo Fi, SettingLineBackup s)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo(Setting.RarPath);
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            var NameZipFile = Fi.DirectoryName + @"\" + Fi.Name + ".rar" +  "." + tmpExt();
            startInfo.Arguments = "a -ri" + s.RarPriority + ":" + s.RarDelay + "  -m5 -ep1 \"" + NameZipFile + "\" \"" + Fi.FullName + "\"" + (s.RarPassword.IsBlank() ? "" : " -p" + s.RarPassword);
            try
            {
                logger.LogInformation($"start arhiving {Fi.FullName}; Length {Fi.Length}");
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
                var fileZip = new FileInfo(NameZipFile);
                mailLoger.AddInfo("Create zip " + fileZip.FullName + "; Length " + fileZip.Length);
                logger.LogInformation($"Create zip {fileZip.FullName}; Length {fileZip.Length}");
                return fileZip;
            }
            catch (Exception e)
            {
                mailLoger.AddError("ERROR " + e.Message + " </br> " + e.StackTrace);
                logger.LogError($"{e.Message}  { e.StackTrace}");
                return null;
            }
        }

        private string tmpExt()
        {
            return "tmpBack";
        }

        private void clearLocalStorage(SettingLineBackup s)
        {
            if (s.LocalFileStorageHours == 0)
                return;

            List<FileInfo> listFindFiles = new List<FileInfo>();
            findFiles(s.PathBackup, "rar", listFindFiles);
            findFiles(s.PathBackup, "complete", listFindFiles);
            listFindFiles.ForEach(f => {
                if (f.Extension == "." + tmpExt()) // должны отправить на фтп. пропустим
                    return;

                if ((DateTime.Now - f.CreationTime).TotalHours > s.LocalFileStorageHours)
                {
                    deleteFile(f);
                    mailLoger.AddInfo("Delete from local " + f.FullName);
                    logger.LogInformation($"Delete from local {f.FullName}");
                }
            });
        }

        private string getPathfromFtp(SettingLineBackup s, FtpClient client, string fileName)
        {
            var res = "";
            client.GetListing(s.PathFTP, FtpListOption.Recursive).ToList().ForEach(item =>
            {
                if (item.Type == FtpFileSystemObjectType.File && item.Name == fileName)
                {
                    res = item.FullName;
                }

            });

            if (res == "")
            {
                res = s.PathFTP + @"/" + (s.EnabledDateFolder ? DateTime.Now.ToString("d") + @"/" : "") + fileName;
            }
            return res;
        }

        private void clearRemoteStorage(SettingLineBackup s)
        {
            if (s.RemoteFileStorageHours == 0)
                return;

            try
            {
                FtpClient client = new FtpClient(Setting.FTPurl);
                if (Setting.FTPlogin.Length != 0)
                    client.Credentials = new NetworkCredential(Setting.FTPlogin, Setting.FTPpassword);
                client.Connect();
                client.GetListing(s.PathFTP, FtpListOption.Recursive).ToList().ForEach(item =>
                    {
                        if (item.Type == FtpFileSystemObjectType.Directory)
                        {
                            FtpListItem[] subItems = client.GetListing(item.FullName, FtpListOption.Recursive);
                            if (subItems.Length == 0)
                            {
                                client.DeleteDirectory(item.FullName);
                                mailLoger.AddInfo("Delete emty folder" + item.FullName);
                                logger.LogInformation($"Delete emty folder {item.FullName}");
                            }
                        }
                        else
                        {
                            if ((DateTime.Now - item.Modified).TotalHours > s.RemoteFileStorageHours)
                            {
                                client.DeleteFile(item.FullName);
                                mailLoger.AddInfo("Delete from ftp " + item.FullName);
                                logger.LogInformation($"Delete from ftp {item.FullName}");
                            }
                        }
                    }
                );
                client.Disconnect();
            }
            catch (Exception e)
            {
                mailLoger.AddError("ERROR clearRemoteStorage" + e.Message + " </br> " + e.StackTrace);
                logger.LogError($"{e.Message}  { e.StackTrace}");
            }
        }

        private bool fileToFTP(FileInfo f, SettingLineBackup s)
        {
            var throttleDispatcher = new ThrottleDispatcher(10000);
            bool res = true;
            try
            {
                FtpClient client = new FtpClient(Setting.FTPurl);
                if (Setting.FTPlogin.Length != 0)
                    client.Credentials = new NetworkCredential(Setting.FTPlogin, Setting.FTPpassword);
                client.Connect();
                client.RetryAttempts = 10;
                string fileFtpName = getPathfromFtp(s, client, getCompleteFileName(f.Name, false));
                client.UploadFile(f.FullName, fileFtpName, FtpRemoteExists.Resume, true, FtpVerify.None, (p) => {
                    throttleDispatcher.ThrottleAsync(async () =>
                    {
                        logger.LogInformation($"progress: {p.Progress} ; sp: {p.TransferSpeed}");
                    });
                }) ;
                mailLoger.AddInfo("Upload to FTP (" + s.PathFTP + ") " + f.FullName + "; Length " + f.Length);
                logger.LogInformation($"Upload to FTP ({s.PathFTP}) {f.FullName}; Length {f.Length}");
                client.Disconnect();

                renameToComplete(f);

                res = true;
            }
            catch (Exception e)
            {
                mailLoger.AddError("ERROR " + e.Message + " </br> " + (e.InnerException == null ? "" : e.InnerException.Message + " </br> ") + e.StackTrace);
                logger.LogError($"{e.Message}  { e.StackTrace}");
                res = false;
            }
            return res;
        }

    }
}
