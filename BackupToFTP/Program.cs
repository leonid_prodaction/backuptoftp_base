﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace BackupToFTP
{
    static class Program
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeConsole();
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            else
            {
                AllocConsole();

                var configuration = new ConfigurationBuilder()
                .AddJsonFile("logsettings.json")
                .Build();

                var services = new ServiceCollection();

                services.AddLogging(builder =>
                {
                    builder.AddConfiguration(configuration.GetSection("Logging"));
                    builder.AddFile(c => c.RootPath = c.RootPath = AppContext.BaseDirectory);
                });

                // create logger factory
                using(var sp = services.BuildServiceProvider())
                {
                    var loggerFactory = sp.GetService<ILoggerFactory>();
                    var toFtp = new ToFTP();
                    toFtp.Run(()=>new MailLogger(),()=> loggerFactory.CreateLogger("logs"));

                }
            }
        }
    }
}
