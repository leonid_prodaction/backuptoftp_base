﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace BackupToFTP
{
    [Serializable]
    public class Setting
    {
        public string FTPlogin { get; set; }
        public string FTPpassword { get; set; }
        public string FTPurl { get; set; }

        /////////////////////////

        public string MailFrom { get; set;}
        public string MailAdressTo { get; set;}
        public string MailPassword { get; set;}
        public string SMTPserver { get; set;}
        public bool UseSSL {get; set;}
        public Int32 MailPort {get; set;}
        public string RarPath { get; set;}

        public string ServerName { get; set; }




        /////////////////////////

        public List<SettingLineBackup> SettingLineBackup;


        public Setting()
        {
            SettingLineBackup = new List<SettingLineBackup>();
        }
    }

    public static class SettingManager
    {
        public static void Save(Setting s)
        {

            // передаем в конструктор тип класса
            XmlSerializer formatter = new XmlSerializer(typeof(Setting));
            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("setting.xml", FileMode.OpenOrCreate))
            {
                fs.SetLength(0);
                formatter.Serialize(fs, s);
            }
        }

        public static Setting Load()
        {
            // return new Setting();
            XmlSerializer formatter = new XmlSerializer(typeof(Setting));
            if (File.Exists("setting.xml"))
            {
                using (FileStream fs = new FileStream("setting.xml", FileMode.OpenOrCreate))
                {
                    return (Setting)formatter.Deserialize(fs);
                }
            }
            else
            {
                return new Setting();
            }
        }
    }

    [Serializable]
    public class SettingLineBackup
    {
        [DisplayName("Путь локального бэкапа")]
        public string PathBackup { get; set; }

        [DisplayName("Отправлять письма")]
        public bool UseMail { get; set; }

        [DisplayName("Путь ФТП")]
        public string PathFTP { get; set; }

        [DisplayName("Расширение файлов бэкапа")]
        public string EXP { get; set; }

        public Int32 RarPriority { get; set; }

        public Int32 RarDelay { get; set; }

        [DisplayName("Пароль RAR")]
        public string RarPassword { get; set; }

        [DisplayName("Делать папки по датам")]
        public bool EnabledDateFolder { get; set; }

        [DisplayName("Очищать ФТП от файлов старше часов")]
        public int RemoteFileStorageHours { get; set; }

        [DisplayName("Очищать Локально от файлов старше часов")]
        public int LocalFileStorageHours { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Архивировать")]
        public bool UseZip { get; set; }
    }

}
