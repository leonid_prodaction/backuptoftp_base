﻿namespace BackupToFTP
{
    interface IMailLogger
    {
        void AddError(string str);
        void AddInfo(string str);
        void AddTitle(string str);
        void SendLog(Setting Setting, string Name = "");
        void Clear();
    }
}