﻿namespace BackupToFTP
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.FTPurl = new System.Windows.Forms.TextBox();
            this.FTPpassword = new System.Windows.Forms.TextBox();
            this.FTPlogin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.MailAdressTo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.UseSSL = new System.Windows.Forms.CheckBox();
            this.MailPort = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SMTPserver = new System.Windows.Forms.TextBox();
            this.MailPassword = new System.Windows.Forms.TextBox();
            this.MailAdress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.RarPuth = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgSetting = new System.Windows.Forms.DataGridView();
            this.Save = new System.Windows.Forms.Button();
            this.dataSetting = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(703, 317);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.FTPurl);
            this.tabPage1.Controls.Add(this.FTPpassword);
            this.tabPage1.Controls.Add(this.FTPlogin);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(695, 291);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Данные ФТП";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // FTPurl
            // 
            this.FTPurl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FTPurl.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "FTPurl", true));
            this.FTPurl.Location = new System.Drawing.Point(151, 69);
            this.FTPurl.Name = "FTPurl";
            this.FTPurl.Size = new System.Drawing.Size(538, 20);
            this.FTPurl.TabIndex = 5;
            // 
            // FTPpassword
            // 
            this.FTPpassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FTPpassword.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "FTPpassword", true));
            this.FTPpassword.Location = new System.Drawing.Point(151, 38);
            this.FTPpassword.Name = "FTPpassword";
            this.FTPpassword.Size = new System.Drawing.Size(538, 20);
            this.FTPpassword.TabIndex = 4;
            // 
            // FTPlogin
            // 
            this.FTPlogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FTPlogin.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "FTPlogin", true));
            this.FTPlogin.Location = new System.Drawing.Point(151, 4);
            this.FTPlogin.Name = "FTPlogin";
            this.FTPlogin.Size = new System.Drawing.Size(538, 20);
            this.FTPlogin.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "FTPurl";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "FTPpassword";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "FTPlogin";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.MailAdressTo);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.UseSSL);
            this.tabPage2.Controls.Add(this.MailPort);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.SMTPserver);
            this.tabPage2.Controls.Add(this.MailPassword);
            this.tabPage2.Controls.Add(this.MailAdress);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(695, 291);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Данные почты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "ServerName", true));
            this.textBox1.Location = new System.Drawing.Point(154, 182);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(533, 20);
            this.textBox1.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(9, 180);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 20);
            this.label11.TabIndex = 18;
            this.label11.Text = "ServerName";
            // 
            // MailAdressTo
            // 
            this.MailAdressTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MailAdressTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "MailAdressTo", true));
            this.MailAdressTo.Location = new System.Drawing.Point(154, 156);
            this.MailAdressTo.Name = "MailAdressTo";
            this.MailAdressTo.Size = new System.Drawing.Size(533, 20);
            this.MailAdressTo.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(9, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "MailAdressTo";
            // 
            // UseSSL
            // 
            this.UseSSL.AutoSize = true;
            this.UseSSL.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.dataSetting, "UseSSL", true));
            this.UseSSL.Location = new System.Drawing.Point(154, 101);
            this.UseSSL.Name = "UseSSL";
            this.UseSSL.Size = new System.Drawing.Size(15, 14);
            this.UseSSL.TabIndex = 15;
            this.UseSSL.UseVisualStyleBackColor = true;
            // 
            // MailPort
            // 
            this.MailPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MailPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "MailPort", true));
            this.MailPort.Location = new System.Drawing.Point(154, 130);
            this.MailPort.Name = "MailPort";
            this.MailPort.Size = new System.Drawing.Size(533, 20);
            this.MailPort.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(9, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "MailPort";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(9, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "UseSSL";
            // 
            // SMTPserver
            // 
            this.SMTPserver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SMTPserver.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "SMTPserver", true));
            this.SMTPserver.Location = new System.Drawing.Point(154, 69);
            this.SMTPserver.Name = "SMTPserver";
            this.SMTPserver.Size = new System.Drawing.Size(533, 20);
            this.SMTPserver.TabIndex = 11;
            // 
            // MailPassword
            // 
            this.MailPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MailPassword.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "MailPassword", true));
            this.MailPassword.Location = new System.Drawing.Point(154, 38);
            this.MailPassword.Name = "MailPassword";
            this.MailPassword.Size = new System.Drawing.Size(533, 20);
            this.MailPassword.TabIndex = 10;
            // 
            // MailAdress
            // 
            this.MailAdress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MailAdress.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "MailFrom", true));
            this.MailAdress.Location = new System.Drawing.Point(154, 4);
            this.MailAdress.Name = "MailAdress";
            this.MailAdress.Size = new System.Drawing.Size(533, 20);
            this.MailAdress.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(9, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "SMTPserver";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(9, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "MailPassword";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "MailAdressFrom";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.RarPuth);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.dgSetting);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(695, 291);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Настрйока бэкапов";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // RarPuth
            // 
            this.RarPuth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RarPuth.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetting, "RarPath", true));
            this.RarPuth.Location = new System.Drawing.Point(148, 257);
            this.RarPuth.Name = "RarPuth";
            this.RarPuth.Size = new System.Drawing.Size(227, 20);
            this.RarPuth.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(3, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "RarPath";
            // 
            // dgSetting
            // 
            this.dgSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSetting.Location = new System.Drawing.Point(3, 3);
            this.dgSetting.Name = "dgSetting";
            this.dgSetting.Size = new System.Drawing.Size(709, 249);
            this.dgSetting.TabIndex = 0;
            this.dgSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSetting_CellContentClick);
            this.dgSetting.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgSetting_DataBindingComplete);
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(481, 331);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(230, 36);
            this.Save.TabIndex = 1;
            this.Save.Text = "Сохранить настройки";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // dataSetting
            // 
            this.dataSetting.DataSource = typeof(BackupToFTP.Setting);
            this.dataSetting.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.dataSetting_BindingComplete);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 383);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Настройка бэкапов";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FTPurl;
        private System.Windows.Forms.TextBox FTPpassword;
        private System.Windows.Forms.TextBox FTPlogin;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox SMTPserver;
        private System.Windows.Forms.TextBox MailPassword;
        private System.Windows.Forms.TextBox MailAdress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox UseSSL;
        private System.Windows.Forms.TextBox MailPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgSetting;
        private System.Windows.Forms.TextBox MailAdressTo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox RarPuth;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.BindingSource dataSetting;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label11;
    }
}

